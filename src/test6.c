#define _DEFAULT_SOURCE
#include "mem_internals.h"
#include "mem.h"

void test6(void *heap) {
    printf("\n\ntest 6: allocating new region non-extension for previous\n");
    struct block_header *block= (struct block_header *) heap;
    while (block->next) {
        block = block->next;
    }
    void *nextPage = (void *) (block->contents + block->capacity.bytes);
    void *res = mmap(nextPage, REGION_MIN_SIZE, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);
    if (res == MAP_FAILED) {
        printf("    unable to proceed test, mmap failed :(\n");
        return;
    }
    char *block_in_new_region = _malloc(32000);
    if (block_in_new_region) {
        *block_in_new_region = 0xF;
    } else {
        printf("    failed, unable to allocate new block :(\n");
        return;
    }
    debug_heap(stdout, heap);
    _free(block_in_new_region);
}
