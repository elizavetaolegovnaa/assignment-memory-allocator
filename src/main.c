#include "mem.h"
#include "test6.h"

int error(const char *err) {
    fprintf(stderr, "test failed! %s\n", err);
    return -1;
}

char *initBlock(int size) {
    static char counter = 1;
    char *block = _malloc(size);
    if (block) *block = counter++;
    return block;
}

int main() {
    void *heap;
    {
        printf("\ntest 1: first allocation\n");
        heap = heap_init(1000);
        if (!heap) return error("heap init error");
        char *block0;
        if (!(block0 = initBlock(1000))) return error("error allocating block.");
        debug_heap(stdout, heap);
        _free(block0);
    }
    {
        printf("\n\ntest 2: freeing 1 block\n");
        char *blocks[3];
        if (!(blocks[0] = initBlock(1000))) return error("error allocating block.");
        if (!(blocks[1] = initBlock(1000))) return error("error allocating block.");
        if (!(blocks[2] = initBlock(1000))) return error("error allocating block.");
        printf("\n    3 blocks allocated\n");
        debug_heap(stdout, heap);
        _free(blocks[1]);
        printf("\n    freed second\n");
        debug_heap(stdout, heap);
        _free(blocks[0]);
        _free(blocks[2]);
    }
    {
        printf("\n\ntest 3: freeing 2 block\n");
        char *blocks[4];
        if (!(blocks[0] = initBlock(1333))) return error("error allocating block.");
        if (!(blocks[1] = initBlock(1333))) return error("error allocating block.");
        if (!(blocks[2] = initBlock(1333))) return error("error allocating block.");
        if (!(blocks[3] = initBlock(1333))) return error("error allocating block.");
        printf("\n    4 blocks allocated\n");
        debug_heap(stdout, heap);
        _free(blocks[0]);
        _free(blocks[2]);
        printf("\n    freed two blocks\n");
        debug_heap(stdout, heap);
        _free(blocks[1]);
        _free(blocks[3]);
    }
    {
        printf("\n\ntest 4: allocating blocks in new region\n");
        char *big_block, *blocks[2];
        if (!(blocks[0] = initBlock(1333))) return error("error allocating block.");
        if (!(big_block = initBlock(8000))) return error("error allocating block.");
        if (!(blocks[1] = initBlock(1333))) return error("error allocating block.");
        debug_heap(stdout, heap);
        _free(blocks[0]);
        _free(big_block);
        _free(blocks[1]);
    }
    {
        printf("\n\ntest 5: splitting blocks\n");
        char *blocks[16], *big_block;
        for (int i = 0; i < 16; ++i) {
            if (!(blocks[i] = initBlock(500))) return error("error allocating block.");
        }
        for (int i = 1; i < 16; ++i) {
            _free(blocks[i]);
        }
        printf("\n    heap before splitting allocation\n");
        debug_heap(stdout, heap);
        if (!(big_block = initBlock(12000))) return error("error allocating block.");
        printf("\n    allocated big block\n");
        debug_heap(stdout, heap);
        _free(blocks[0]);
        _free(big_block);
    }

    test6(heap);

    {
        printf("\n\ntest 7: last big allocation\n");
        char *big_block, *big_block2;
        if (!(big_block = initBlock(16350))) return error("error allocating block.");
        if (!(big_block2 = initBlock(320000))) return error("error allocating block.");
        debug_heap(stdout, heap);
        _free(big_block);
        _free(big_block2);
    }
    {
        printf("\n\ntest 8: empty heap\n");
        debug_heap(stdout, heap);
    }
    return 0;
}
